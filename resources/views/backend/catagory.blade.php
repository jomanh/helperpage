@extends('backend.admin.layout')
@section('content')
    <section id="input-style">
        <div class="row">
            <div class="col-12">
                <div class="main-card mb-3 card">

                    <div class="card-content">
                        <div class="card-body">
                            <h4>
                                Blog Category
                            </h4>

                            <div class="row" style="margin: 20px;">
                                <div class="col-md-6">
                                    <button type="button" id="addClick" class="btn btn-outline-warning rounded-pill mb-3"
                                        data-toggle="modal">ADD</button>

                                </div>
                            </div>
                            <div class="row">

                                {{-- for the up down on right --}}
                                {{-- style="overflow-x:auto;" --}}

                                <div class="col-sm-12" >
                                    <table style="width: 100%;" id="example"
                                        class="table table-hover table-striped table-bordered data-table">
                                        <thead>

                                            <tr>
                                                <th> #</th>
                                                <th> Name </th>
                                                <th>Image </th>
                                                <th> Action </th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@push('pageJs')


<div class="modal fade text-left " id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="title">Blog Category</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>

            </button>
        </div>
        <form action="#" method="post" id="editFromData">
            <form action="#" method="post" id="editFromData">
                @csrf
                <input type="hidden" name="_id" id="_id">
                <div class="modal-body">
                    <br>
                    <div class="row">
                        <div class="col-md-6"> <label> Name </label>
                            <div class="form-group">
                                <input type="text" name="name" id="name" placeholder=""
                                    class="form-control">

                            </div><br>
                            <label for="roundText"> Image</label>
                            <div class="container">
                                <section id="draggable-cards">
                                    <div class="row match-height" id="card-drag-area">
                                    </div>

                                </section>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <div class="needsclick dropzone" id="document-dropzone">
                                </div> <br>
                            </fieldset>
                        </div>
                    </div>

                </div>



                <div class="modal-footer">
                    <input type="reset" class="btn bg-light-secondary" data-bs-dismiss="modal" value="close">
                    <input type="submit" id="saveBtn" class="btn btn-primary" value="save">
                   </div>
            </form>
    </div>
</div>
</div>
    {{-- dropzone --}}

    <script>
        Dropzone.autoDiscover = false;
        var uploadedDocumentMap = {}
        var alesDropZone = new Dropzone("#document-dropzone", {
            url: '{{ route('projects.storeMedia', ['table' => 'categories']) }}',
            maxFiles: 1,
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            processing: function() {
                $('#saveBtn').prop('disabled', true);
                toastr.options.positionClass = 'toast-top-center';
                toastr.warning('wait until finish uploading  ');
            },
            init: function() {
                this.on('addedfile', function(file) {
                    if (this.files.length > 1) {
                        this.removeFile(this.files[1]);
                        toastr.options.positionClass = 'toast-top-center';
                        toastr.success('only one image can be uploded');
                    }
                });
                // this.hiddenFileInput.removeAttribute('multiple');
            },
            success: function(file, response) {
                $('#editFromData').append('<input type="hidden" name="image" value="' + response.name + '">')

                uploadedDocumentMap[file.name] = response.name
                $('#saveBtn').prop('disabled', false);
            },
            removedfile: function(file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $('#editFromData').find('input[name="image"][value="' + name + '"]').remove()
            },

        });
    </script>
    <script type="text/javascript">
        $(function() {


            table = $('.data-table').DataTable({

                destroy: true,
                processing: true,
                serverSide: true,
                stateSave: true,

                ajax: "{{ route('catagory.index') }}",

                columns: [

                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'image',
                        name: 'image'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    },

                ]

            });


            $("#addClick").click(function(e) {
                e.preventDefault();
                $("#saveBtn").html("add");
                $('#_id').val('');
                $('#editFromData').trigger("reset");
                // $("#exampleModal").css("z-index", 1088);
                $("#inlineForm").modal("show");
                alesDropZone.removeAllFiles(true);
                $("#image_sub").html("");
                $("#card-drag-area").html("");
            });


            $('body').on('click', '.edit', function() {

                var catagory_id = $(this).data('id');
                $("#title").html("Update ");
                $.get("{{ route('catagory.index') }}" + '/' + catagory_id + '/edit', function(data) {
                    $("#saveBtn").val("update");
                    $('#_id').val(data.id);
                    $('#name').val(data.name);
                    var icon_s = "<img src='{{ asset('/storage/backend') }}/" + data.image +
                        "' width='120' height='120'>";
                    $("#card-drag-area").html(icon_s);
                    alesDropZone.removeAllFiles(true);
                    $("#inlineForm").modal('show');
                })


            }); // end edit function;

            $("#saveBtn").click(function(e) {
                e.preventDefault();

                $("#saveBtn").html(' saving ..');
                $("#saveBtn").attr('disabled', true);
                var catagory_id = $("#_id").val();
                var method = "post";
                var url = "{{ route('catagory.store') }}";
                if (catagory_id) {
                    url = "{{ route('catagory.index') }}" + '/' + catagory_id
                    method = "PATCH";
                }
                $.ajax({

                    data: $('#editFromData').serialize(),

                    url: url,

                    type: method,

                    dataType: 'json',
                    timeout: 4000,
                    success: function(data) {
                        $("#saveBtn").html(' save');
                        $("#saveBtn").attr('disabled', false);
                        $('#editFromData').trigger("reset");
                        showSuccesFunction();
                        $("#inlineForm").modal('hide');
                        table.draw(false);
                    },

                    error: function(data) {
                        $("#saveBtn").html(' save');
                        $("#saveBtn").attr('disabled', false);
                        showErrorFunction();
                        $("#inlineForm").modal('hide');

                    }

                });
            }); // end save , update record data



            $('body').on('click', '.delete', function() {
                var catagory_id = $(this).data("id");
                sweetConfirm(function(confirmed) {
                    if (confirmed) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('catagory.index') }}" + '/' + catagory_id,
                            data: {
                                '_token': '{{ csrf_token() }}'
                            },
                            success: function(data) {
                                showSuccesFunction();
                                table.draw(false);
                            },
                            error: function(data) {}
                        });
                    }

                });

            }); // end delete row


        });
    </script>
@endpush
