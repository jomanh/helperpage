
<!DOCTYPE html>
<html  data-head-attrs=""  lang="ar" dir="rtl">

<head>
  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="Arabic">

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />


<title>تقرير بالزيارات عبر google Analytics</title>
<link rel="canonical" href="">
</head>

<body  data-head-attrs="" >
  <div>
    <main class="help-center-content">
      <section class="section section--home mt-0 mb-0">
        <!----><div class="post-content">
          <p><strong><span style="color:  rgb(38, 123, 189);">تقرير بالزيارات عبر google Analytics:</span></strong> هو تقرير يتم عرضه في لوحة تحكم متجرك، ويعرض <span>معلومات حول: المدن، الأجهزة، الصفحات، ومصادر الزيارات لمتجرك

          </span> بالاعتماد على حساب المتجر على &nbsp;google Analytics.</p>
          <p>
            <br>
          </p><p><strong><span style="color: rgb(38, 123, 189);">خطوات عرض تقرير بالزيارات&nbsp;</span></strong><span style="color:  rgb(38, 123, 189);">
            <strong>عبر google Analytics:</strong></span></p><p dir="RTL"><span lang="AR-SY">1. من القائمة الجانبية للوحة التحكم، اضغط على (التقارير)</span></p><p dir="RTL">
              <img src="" alt="" width="185" height="41" class=""></p><p dir="RTL">&nbsp;</p><p dir="RTL"><span lang="AR-SY">2. انتقل عبر علامات التبويب لتحديد نوع التقرير وعرضه، اضغط(الزيارات ).</span></p><p dir="RTL"><img src="" style="width: 550px;" class="">
            </p><p dir="RTL">
              <br></p><p dir="RTL">3. استعرض تقارير google Analytics، والتي تتضمن معلومات حول المدن، الأجهزة، الصفحات، ومصادر الزيارات لمتجرك.</p><p dir="RTL">
                <img src="" style="width: 550px;" class=""></p><p dir="RTL"><br></p><p dir="RTL">ولطباعة التقرير، اضغط على زر (طباعة) في أعلى نافذة التقارير.<img src="" style="width: 550px;" class=""></p><p dir="RTL"><br></p>
                <p dir="RTL"><br></p><h2><span style="color:  rgb(38, 123, 189);">الأسئلة الشائعة</span></h2>
                <p>سنستعرض إجابات الأسئلة التالية:</p>
                <ul>
                  <li>ما الخطوات لربط متجري مع حسابي على google Analytics؟</li>
                  <li>هل يمكنني إلغاء ربط متجري بحسابي على Google Analytics ؟</li>
                  <li>هل احتاج لربط متجري من جديد، علماً أنني قمت بالربط قبل تحديث الميزة؟</li>
                  <li>ما هي المدة الزمنية التي سيتم عرض تقرير الزيارات وفقها؟</li>
                </ul>
                <p>
                  <br>
                </p>
                <p><strong><span style="color:  rgb(38, 123, 189);">ما الخطوات لربط متجري مع حسابي على google Analytics؟</span></strong>
                </p>
                <p>في حال<strong>&nbsp;لم يكن لديك حساب Google Analytics</strong> حتى الآن، اضغط لمزيد من التفاصيل حول <a href="">
                  <strong>خطوات التسجيل في Google Analytics</strong></a></p><p><br>
                </p><p>في حال لم تكن قد ربطت متجرك مع حسابك على google Analytics سيظهر في نافذة الزيارات رسالة تفيد بعدم وجود بيانات في الوقت الحالي.</p>
                <p>1. لربط المتجر بحسابك على <span>google Analytics،&nbsp;</span>اضغط (تفعيل الربط).</p><p>
                  <img src="" style="width: 550px;" class="fr-fic fr-dib fr-fir">
                </p>
                <p>2. فيتم فتح صفحة التطبيقات المثبتة، اضغط (إعدادات التطبيق) أسفل Google Analytics.</p><p >
                  <img src="" style="width: 450px;" class="fr-fic fr-dib fr-fir"></p><p ><br></p><p >
                    <strong>ملاحظة هامة</strong>: إذا لم تكن قد ثبتت تطبيق <span>Google Analytics</span> حتى الآن، فتوجه إلى متجر تطبيقات سلة وثبته</p>
                <p ><img src="" style="width: 550px;"></p>

                <p >لمزيد من التفاصيل حول <a href=""><strong>تثبيت تطبيق من متجر تطبيقات سلة</strong></a></p><p>
                  <br>
                </p>
                <p >3. &nbsp;من صفحة بيانات الربط مع <span>Google Analytics، اختر ربط البيانات من القائمة.</span></p><p><span>
                  <img src="" style="width: 550px;"></span>
                </p><p><br>
                </p><p>هذه &nbsp;الروابط مرسلة من <span>Google Analytics</span> بعد الربط، وهي حساباتك على <span style="color: rgb(65, 65, 65);">Google Analytics.</span></p>
                <p><span>وقد تكون رابط واحد أو عدة روابط، كما في الشكل:</span>
                </p><p>
                  <img src="" style="width: 550px;"></p>
                <p>يمكنك اختيار أي منها.</p><p><br></p><p>4. بعد اختيار رابط الحساب، اضغط (حفظ)</p><p><img src=""></p>
                <p><br>
                </p>
                <p><br></p>
                <p><strong><span style="color: rgb(38, 123, 189);">هل يمكنني إلغاء ربط متجري بحسابي على</span></strong>
                  <span style="color: rgb(38, 123, 189);"><strong>
                   <span>Google Analytics</span>
                  </strong></span><strong><span style="color:rgb(38, 123, 189);">؟</span></strong></p>
                  <p>نعم، من صفحة الربط مع &nbsp;<span>Google Analytics</span> اضغط (إلغاء الربط).</p>
                  <p ><img src="" style="width: 550px;"></p>
                  <p ><br></p>
                  <p >ويمكنك إعادة الربط مع أي حساب آخر على <spa>Google Analytics</span> في أي وقت تريد.</p>
                    <p><br>
                    </p><p><strong><span style="color: rgb(38, 123, 189);">هل احتاج لربط متجري من جديد، علماً أنني قمت بالربط قبل تحديث الميزة؟</span></strong></p>
                    <p>نعم، تحتاج لإعادة الربط من جديد.</p><p><br></p><p><strong>
                      <span style="color: rgb(38, 123, 189);">ما هي المدة الزمنية التي سيتم عرض تقرير الزيارات وفقها؟</span></strong>
                    </p>
                    <p>سيتم عرض تقرير الزيارات لأخر 30 يوم فقط.</p>
                    <p>
                      <br>
                    </p>
                  </div><!----><!---->

                  </main>
                  </div>


</body>

</html>
