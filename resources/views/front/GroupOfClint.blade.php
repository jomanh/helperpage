GroupOfClint

<!DOCTYPE html>
<html  data-head-attrs=""  lang="ar" dir="rtl">

<head>
  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="Arabic">
  <!--fav icon-->
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-152x152.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

  <!--fav icon-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!-- Google Tag Manager -->



  <link rel="manifest" href="/manifest.webmanifest">
<title>مجموعات العملاء</title><link rel="canonical" href=""><meta name="title" content="مجموعات العملاء"><meta name="description" >

<body  data-head-attrs="" >
  <!----><div>
    <p><strong><span style="color:  rgb(38, 123, 189)">ما هي مجموعات العملاء؟</span></strong></p><p>تمكنك من تقسيم العملاء بشكل آلي حسب (عدد طلباتهم، وإجمالي مشترياتهم، وتقييمهم للمتجر) ، أو بشكل يدوي من خلال تحديدهم ثم إضافتهم للمجموعة المناسبة، ومن ثم يمكنك وضع مزايا مستقلة لكل مجموعة بشكل خاص (طرق الدفع المتاحة، وخيارات الشحن، وكوبونات التخفيض)، مع العلم أنه يمكنك إدراج العملاء تحت أكثر من مجموعة.</p><p>&nbsp;</p><p><strong>
      <span style="color:  rgb(38, 123, 189)">خطوات إنشاء مجموعات عملاء جديدة:</span></strong></p><p>1. من القائمة الجانبية للوحة التحكم، اضغط على (العملاء).</p>
    <p>
        <img src="" alt="" width="150" height="27" class=""></p><p>&nbsp;</p>

      <p>2. من صفحة العملاء، اضغط على (+ مجموعة جديدة).</p><p>
        <img src="" alt="" width="550" height="170" class="">
      </p><p>&nbsp;</p><p>3. أدخل بيانات المجموعة، ثم اضغط على (حفظ).</p>
      <p><img src="" style="width: 450px;" class=""></p>
      <p style="text-align: right;">1- رمز واسم المجموعة، أدخل اسمًا قصيرًا ومعبرًا للمجموعة مع اختيار الرمز المناسب.</p>
      <p>2- شرط إضافة العملاء تلقائيًا إلى المجموعة: سيتم إضافة أي عميل يحقق الشرط بشكل تلقائي، ويمكنك ترك الشرط ولكنك ستضطر إلى إضافة العملاء إلى المجموعة بشكل يدوي.</p>
      <p>3- حدد مزايا المجموعة من خلال:</p>
      <p>&nbsp; &nbsp; - الحماية من الاحتيال: يمكن التحكم بطرق الدفع التي تريد لها أن تظهر لعملاء المجموعة.</p>
      <p>&nbsp; &nbsp; - قيود شركات الشحن: يمكن التحكم بشركات الشحن التي تريد لها أن تظهر لعملاء المجموعة.</p><p>&nbsp;</p>
        <h2><span style="color:  rgb(38, 123, 189);">الأسئلة الشائعة</span>
      </h2><p>سنستعرض الأسئلة الشائعة التالية:</p>
      <ol>
        <li>كيف يمكنني تحديد شرط إضافة العملاء تلقائيًا للمجموعة؟</li>
        <li>كيف يمكنني التحكم بطرق الدفع التي تظهر للمجموعة؟</li>
        <li>كيف يمكنني التحكم بظهور شركات الشحن للمجموعة؟</li>
        <li>كيف يمكنني حذف مجموعة عملاء؟</li>
        <li>كيف يمكنني تعديل رمز مجموعة العملاء؟</li>
      </ol>
      <p><br></p>
      <p><strong>
        <span style="color:  rgb(38, 123, 189)">كيف يمكنني تحديد شرط إضافة العملاء تلقائياً للمجموعة؟</span></strong>
      </p><p style="text-align: right;">ينقسم شرط إضافة العملاء تلقائيًا إلى المجموعة إلى ثلاثة أقسام:</p>
      <p>
        <img src="" alt="" width="550" height="60">
      </p>
      <p>1- نوع الشرط</p>
      <p>تحديد نوع شرط إضافة العملاء بشكل تلقائي للمجموعة، وتتضمن: عدد طلبات العميل، وإجمالي مشتريات العميل، وتقييم العميل للمتجر، والعملاء الذين ليس لديهم طلبات.</p><p>على سبيل المثال، سنحدد نوع الشرط إلى: عدد الطلبات.</p>
      <p>
        <img src="" style="width: 450px;" class="fr-fic fr-dib fr-fir">
      </p>
      <p>
        <br>
      </p><p><br>
      </p><p>2- الشرط، تحديد الشرط (أكبر من)، أو (أصغر من)، أو (ضمن نطاق).</p><p>&nbsp;<img src=""></p>
      <p style=>&nbsp;</p><p>3- قيمة الشرط، اكتب قيمة الشرط.</p>
      <p>ففي حالة الشرط (ضمن نطاق) نكتب أقل قيمة وأكبر قيمة لتحقق الشرط، كما في الشكل:</p><p>
        <img src="" style="width: 550px"></p>
        <p><br></p>
        <p>وفي حال اختيار الشرط (أكبر من) أو ( أصغر من)؛ فنكتب القيمة التي تحقق الشرط.</p>
        <p>وفي المثال التالي حددنا القيمة إلى: 5، وأي عدد للطلبات يتجاوز 5 طلبات سيحقق الشرط.</p><p><img src="" style="width: 550px;" class="fr-fic fr-dib fr-fir"></p>
        <p>&nbsp;</p>
        <p><strong><span style="color:  rgb(38, 123, 189)">كيف يمكنني التحكم بطرق الدفع التي تظهر للمجموعة؟</span></strong></p><p>من صفحة مجموعة العملاء المطلوبة، اضغط على (الحماية من الاحتيال).</p>
        <p><img src="" style="width: 450px;" class="">
        </p><p><br></p><p>أدخل قيد دفع جديد لمجموعة العملاء المطلوبة، ثم اضغط على (حفظ).</p>
        <p><img src="" style="width: 550px;" class=""></p>
        <p>لمزيد من التفاصيل حول <a href=""><strong>إعدادات الحماية من الاحتيال</strong></a></p>
        <p><br>
        </p><p>
          <strong>
            <span style="color:  rgb(38, 123, 189)">كيف يمكنني التحكم بظهور شركات الشحن للمجموعة؟</span>
          </strong></p><p>من صفحة مجموعة العملاء المطلوبة، اضغط على (قيود شركات الشحن).</p><p>
          <img src="" style="width: 450px;"></p>
          <p>
            <br></p>
            <p>أدخل قيدًا جديدًا لشركات الشحن إلى مجموعة العملاء، ثم اضغط على (حفظ).</p><p><img src="" style="width: 550px;" class="fr-fic fr-dib fr-fir"></p>
            <p>لمزيد من التفاصيل حول <a href=""><strong>قيود شركات الشحن</strong>
            </a></p><p><br></p><p>
              <strong><span style="color:  rgb(38, 123, 189)">كيف يمكنني حذف مجموعة عملاء؟</span>
              </strong></p><p>من خلال الضغط على (حذف) من صفحة المجموعة.</p>
              <p><img src="" style="width: 450px;" class="fr-fic fr-dib fr-fir">
              </p>
              <p>ملاحظة هامة: عند حذف مجموعة عملاء، لن يتم حذف سجلات العملاء الذين كانوا ضمنها.</p>
              <p>&nbsp;</p><p><br></p><p><strong>
                <span style="color:  rgb(38, 123, 189)">كيف يمكنني تعديل رمز مجموعة العملاء؟</span></strong></p><p style="text-align: right;">من صفحة المجموعة، اضغط على رمز المجموعة واختر الرمز المناسب من قائمة الرموز.</p>
                <p style="text-align: right;"><img src="" alt="" width="250" height="386" class="fr-fic fr-dii">

              </div><!----><!---->
</body>

</html>
