<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('front/style.css')}}">
    <style>

    </style>
    <title> {{ $blog->title }} </title>
    <style>
    </style>
</head>
<body>
<!-- Header -->

<section id="header">
    <div class="header container">
        <div class="nav-bar">
            <div class="brand">
                <div href="#hero">
                    <a href="{{url('/')}}"> <img
                            style="height: 50px;width: 100px;object-fit: contain;object-position: center"
                            src="{{asset('front/logo.png')}}" alt="logo" class="logo"> </a>
                    <i class="fas fa-bars" id="ham-menu"></i>
                </div>
            </div>
            <div class="nav-list">
                <div class="hamburger">
                    <div class="bar"></div>
                </div>
                <ul>
                    <li><a href="" data-after="/callus">اتصل بنا</a></li>
                    <li><a href="" data-after="/Service"> من نحن</a></li>
                    <li><a href="" data-after="/quastion">الاسئلة الشائعة</a></li>
                    <li><a href="" data-after="/English">English</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- End Header -->
<!-- Hero Section  -->
<section id="hero">
    <div class="hero container">
        <div>

            <h1>اهلا, <span></span></h1>
            <h1>كيف نقدر نساعدك <span></span></h1>
            <h1>اليوم! <span></span></h1>
            <div href="#projects" placeholder="ابحث هنا" aria-label="Search" aria-describedby="search-addon">


                <form class="example" action="{{route('blog.search')}}" method="post">
                    @csrf
                    <input type="text" placeholder="ابحث عما تريد" name="search">
                    {{--              <input type="submit" value="Search">--}}
                </form>
                <a href="#projects" type="button" class="cta">تسجيل دخول</a>
            </div>
        </div>
    </div>
</section>
<section id="contact">
    <div class="contact container">
        <div class="contact-items">
            <div class="contact-item">
                <div class="icon"><img src="{{asset('front/video.png')}}"/></div>
                <div class="contact-info">

                    <h1>تعلم خطوة بخطوة</h1>
                    <h2>حول اقسام متجرك</h2>

                </div>
            </div>
            <div class="contact-item">
                <div class="icon"><img src="{{asset('front/hint.png')}}"/></div>
                <div class="contact-info">
                    <h1>تلميحات فاتورة</h1>

                    <h2>تعلم خطوة بخطوة</h2>
                    <h2>من خلال الفديو حول </h2>
                    <h2>اقسام متجرك والادوات</h2>
                </div>
            </div>
            <div class="contact-item">
                <div class="icon"><img src="{{asset('front/add-user.png')}}"/></div>
                <div class="contact-info">
                    <h1>انشاء حساب</h1>
                    <h2>اضغط هنا لإنشاء حساب جديد في فاتورة</h2>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Service Section -->
<section style="direction: rtl;">
    <section class="bar">

        <div class="cont" style=" padding-right: 30px">

        <h2 style="text-align: center">
            {{ $blog->title }}
        </h2>
            <div style="text-align: center">
                {{ $blog->cat->name }}
            </div>
            @if( $blog->image)
            <div style="text-align: center" >
                <img src="{{asset('storage/backend/' . $blog->image)}}" style="max-height: 300px;" alt="">
            </div>
            @endif
            <div style="text-align: center">
                {!! $blog->description !!}
            </div>


        </div>
    </section>
    <br>
    <!-- <button class="accordion">المنتجات<img src="" style="height: 20px;width: 20px p"height: 50px;width: 70px></button>-->
    <!-- End Contact Section -->
    <!-- Footer -->
    <section id="footer">
        <div class="footer container">
            <div class="brand">
                <h1><span> f</span>atorah <span>p</span>ro</h1>
            </div>
            <h2>فاتورة برو</h2>
            <div class="social-icon">
                <div class="social-item">
                    <a href="#"><img src="https://img.icons8.com/bubbles/100/000000/facebook-new.png"/></a>
                </div>
                <div class="social-item">
                    <a href="#"><img src="https://img.icons8.com/bubbles/100/000000/instagram-new.png"/></a>
                </div>


            </div>
            <p>Copyright © 2023 fatorah pro</p>
        </div>
    </section>
</section>
    <!-- End Footer -->
    <script src="{{asset('front/final2.js')}}"></script>
</body>

</html>



