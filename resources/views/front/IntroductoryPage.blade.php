
<!DOCTYPE html>
<html  data-head-attrs=""  lang="ar" dir="rtl">

<head>
  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="Arabic">
  <!--fav icon-->
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-152x152.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">


  <meta name="theme-color" content="#5CD5C4">
</head>
<body>
      <p>
        <strong>
        <span style="color: rgb(38, 123, 189);">ما هي الصفحات التعريفية؟</span>
      </strong>
    </p>
      <p>هي صفحات يتم إنشاؤها للتعريف بالمتجر (صفحة من نحن، اتفاقية الاستخدام، الشروط والخصوصية،..)، أو للتعريف بأحد المكونات التي تستخدمها في الإنتاج من خلال وصف مفصل ودقيق حوله.

      </p>
      <p >&nbsp;</p>
      <p><strong>
        <span style="color:rgb(38, 123, 189)">خطوات إنشاء صفحة تعريفية على متجرك:</span>
      </strong>
      </p>
      <p>1. من القائمة الجانبية للوحة التحكم، اضغط على (الصفحات التعريفية).</p>
      <p >
        <img src="" alt="" width="221" height="45">
      </p>
      <p>&nbsp;</p>
      <p>2. لإنشاء صفحة تعريفية جديدة، اضغط على (صفحة جديدة).</p>
        <p>
          <img src="" alt="" width="498" height="137"></p><p>&nbsp;</p>
        <p>3. اكتب عنوان الصفحة، ومحتوياتها بما في ذلك إدراج الصور ومقاطع الفيديو وروابط صفحات على الإنترنت وغيرها.</p>
        <p><img src="" style="width: 550px;"></p>
        <p><br></p>
        <p>4. تحديد نوع الصفحة (عامة، سياسة الخصوصية، سياسة الاستبدال والاسترجاع)</p>
        <p><img src="" style="width: 450px;"></p><p>&nbsp;</p>
        <p>5. اضغط على (الحفظ) لحفظ الصفحة والتحكم بظهورها.</p>
        <p><img src="" alt="" width="500" height="40"></p>
        <p>&nbsp;</p>
        <h2><strong><span style="color: rgb(38, 123, 189)">الأسئلة الشائعة</span>
      </strong>
    </h2>
    <p>سنستعرض إجابات الأسئلة التالية:</p>
    <ol>
      <li>كيف يمكنني إعادة ترتيب الصفحات التعريفية في المتجر؟</li>
          <li>كيف يمكنني إيقاف عرض صفحة تعريفية دون حذفها؟</li>
          <li>كيف يمكنني حذف صفحة تعريفية؟</li>
          <li>هل يمكنني نسخ رابط الصفحة التعريفية؟</li>
          <li>هل هناك نصوص مساعدة لسياسات الاستبدال أو الخصوصية؟</li><li>ما الهدف من تحديد صفحة تعريفية كصفحة سياسة الاستبدال والاسترجاع؟</li></ol>
          <p><strong>
            <span style="color: rgb(38, 123, 189)">كيف يمكنني إعادة ترتيب الصفحات التعريفية في المتجر؟</span>
          </strong>
        </p>
        <p>&nbsp;لترتيب عرض الصفحات التعريفية في المتجر، توجه إلى (خدمات) واضغط (ترتيب عرض الصفحات التعريفية).</p><p style="direction: rtl;">
            <img src="">
          </p>
          <p>وابدأ بترتيب الصفحات بالسحب والإفلات ثم اضغط (حفظ التعديلات).</p><p><img src=""></p>
            <p><br>
            </p><p ><strong><span style="color: rgb(38, 123, 189)">كيف يمكنني إيقاف عرض صفحة تعريفية دون حذفها؟</span></strong></p>
            <p>لتعطيل عرض صفحة تعريفية، اضغط على الصفحات التعريفية من القائمة اليمنى، ثم عطل مفتاح عظهور الصفحة التعريفية، كما في الشكل</p><p style="direction: rtl;">
              <img src="" style="width: 550px;"></p><p>&nbsp;</p>
              <p><br></p><p><strong>
                <span style="color: rgb(38, 123, 189)">كيف يمكنني حذف صفحة تعريفية؟</span>
              </strong>
            </p>
            <p style="direction: rtl;">1. من القائمة الجانبية للوحة التحكم، اضغط على (الصفحات التعريفية).</p>
                <p><img src="" alt="" width="221" height="45">
                </p><p>&nbsp;</p>
                <p>2. اضغط على الصفحة التعريفية المراد حذفها.</p><p>
                  <img src="" alt="" width="600" height="103">
                </p><p style>&nbsp;</p>
                <p>3. اضغط على (حذف الصفحة) الموجود أسفل الصفحة التعريفية.</p><p>
                  <img src="" alt="" width="600" height="48" class="fr-fic fr-dii"></p>
                  <p><br>
                  </p><p>
                    <strong>
                      <span style="color: rgb(38, 123, 189)">هل يمكنني نسخ رابط الصفحة التعريفية؟</span>
                  </strong>
                </p>
                <p>نعم، من صفحة الصفحات التعريفية، اضغط (...) بجوار الصفحة التعريفية المطلوبة، ثم اضغط (نسخ الرابط).</p>
                  <p><img src=""></p><p><br></p><p><span ><strong>هل هناك نصوص مساعدة لسياسات الاستبدال أو الخصوصية؟</strong>
                  </span><strong><span style="color: rgb(38, 123, 189)">&nbsp;</span></strong></p><p><strong>نعم</strong>، فعند تحديد نوع الصفحة:</p><p>&nbsp; &nbsp; - سياسة الاستخدام والخصوصية</p>
                  <p>&nbsp; &nbsp; - سياسة الاستبدال</p><p>سيظهر نص افتراضي لهذه السياسات، يُشكل نقطة إنطلاقة لك لكتابة سياسات متجرك.</p>
                  <p><img src="" style="width: 550px;"></p>
                  <p><br></p><p><strong><span style="color: rgb(38, 123, 189)">ما الهدف من تحديد صفحة تعريفية كصفحة سياسة الاستبدال والاسترجاع؟</span>
                  </strong></p><p>حتى يظهر رمز الباركود في الفاتورة، ويتمكن العميل من استعراض صفحة سياسة الاستبدال والاسترجاع بمجرد مسحه للباركود، وكل ذلك حتى يستوفي متجرك واحد من أهم شروط الفاتورة في نظام التجارة الإلكترونية.</p>
                  <p>حيث تظهر الفاتورة بالشكل:</p><p><img src="" style="width: 350px;"></p>
                  <p>
                    <br>
                </p>
                </div>


</body>

</html>
