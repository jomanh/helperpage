
<!DOCTYPE html>
<html  data-head-attrs=""  lang="ar" dir="rtl">
<head>
  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="Arabic">
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-152x152.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
  <meta name="msapplication-TileColor" content="#ffffff">
<title>إضافة أول منتج</title>

<style>

  p{
    color: #000;

  }
  line{
   color: rgb(38, 123, 189);
  }
  body{
    text-align: right;

  }
</style>
</head>
<body  data-head-attrs="" >
  <div id="app" data-server-rendered="true"><main class="help-center-content">
    <section class="section section--home mt-0 mb-0">
                          <div class="mobile-nav__articles">
                              <article class="single-post-wrapper">
                               <div class="post-content"><p>
                                    <strong>
                                      <span  class="line" style="color: rgb(38, 123, 189);">خطوات إضافة أول منتج:</span> </strong> </p>
                                    1. من الصفحة الرئيسية لتأسيس المتجر، اضغط (أضف المنتج).</p>
                                    <p><img src="" alt="photo" style="width: 450px;"></p>
                                    <p>أو من القائمة الجانبية للوحة التحكم، اضغط (المنتجات).</p>
                                    <p><img src="" alt="photo" style="width: 150px;" ></p>
                                    <p><br> </p><p dir="RTL">2. من صفحة المنتجات، اضغط (+منتج جديد).</p> <p dir="RTL" style="text-align: right;">
                                      <img src="" alt="photo" ></p><p dir="RTL"> <br></p><p>3. حدد نوع المنتج، سنعمل على بطاقة منتج جاهز
                                      <span style="color: rgb(55, 23, 136); ">على سبيل المثال </span> .</p>
                                        <p> <img src="" alt="photo"  class=""> <br></p>
                                          <p >4. ادخل البيانات الأساسية للمنتج، ثم اضغط (حفظ).</p> <p>
                                            <img src="" alt="photo" style="width: 300px;" class="fr-fic fr-dii"> </p><p ><br> </p>
                                          <p>1- إضافة صورة أو أكثر للمنتج، لمزيد من التفاصيل حول
                                            <a href=''><strong>إضافة صور وفيديوهات المنتج</strong></a></p>
                                          <p >2- اسم المنتج، اسم المنتج الذي سيظهر للعميل.</p>
                                          <p>3- سعر المنتج، وهو سعر بيع المنتج.</p>
                                          <p>4- كمية المنتج، وهي الكمية المتوفرة من المنتج.</p>
                                          <p>5- تصنيف المنتج، ويساعد على تخصيص عرض المنتجات، عرض تقارير المنتجات، وغير ذلك.</p>
                                          <p>6- تفاصيل المنتج، لتخصيص تفاصيل المنتج كالكمية، السعر، رمز الباركود، وغيرها.</p><p>
                                        &nbsp;بعد إدخال البيانات الرئيسة للمنتج
                                        <span >اضغط (حفظ)</span> ؛ وذلك لتتمكن من التحكم ببيانات المنتج الأخرى من نافذة (بيانات المنتج).
                                      </p><p><br></p>
                                      <h2 style="direction: rtl;">
                                        <span lang="AR-SY" style="color: rgb(38, 123, 189);">
                                          <strong>الأسئلة الشائعة
                                          </strong>
                                        </span>
                                      </h2>
                                      <p style="text-align: right;">سنستعرض إجابات الأسئلة التالية:</p>
                                      <p>1- كيف يمكنني تغيير طريقة عرض بطاقات المنتجات في صفحة المنتجات؟</p>
                                      <p>2- كيف يمكنني تعيين خيارات المنتج والكمية المتوفرة؟</p>
                                        <p>3- كيف يمكنني التحكم ببيانات المنتج من نافذة (بيانات المنتج)؟

                                        </p>
                                        <p>4- كيف يمكنني تثبيت منتج في الصفحة الرئيسية؟</p>
                                        <p>5- ما هي الخيارات التي يتيحها لي زر المزيد؟</p>
                                        <p style="text-align: right;">
                                        <br>
                                        </p>
                                        <p style="text-align: right;">
                                        <strong><span style="color: rgb(38, 123, 189);">كيف يمكنني تغيير طريقة عرض بطاقات المنتجات في صفحة المنتجات؟</span>
                                        </strong>
                                        </p>
                                        <p style="text-align: right;">يمكنك تغيير طريقة عرض المنتجات بما يتوافق مع رغبتك بطريقتين، هما:</p>
                                        <p style="text-align: right;">1. طريقة عرض البطاقات بشكل طولي، كما في الشكل التالي:</p><p style="text-align: right;">
                                        <img src="" alt="photo" style="width: 550px;" class="fr-fic fr-dii">
                                        </p>
                                        <p style="text-align: right;">
                                        <br>
                                        </p>
                                        <p style="text-align: right;">2. طريقة عرض البطاقات بشكل عرضي، كما في الشكل التالي:</p>
                                        <p style="text-align: right;"><img src="" style="width: 550px;"
                                         class="fr-fic fr-dii">
                                         </p>
                                         <p style="text-align: right;">
                                         <br>
                                         </p>
                                         <p style="text-align: right;">
                                         <span style="color: rgb(38, 123, 189)">
                                         <strong>كيف يمكنني تعيين خيارات المنتج والكمية المتوفرة</strong>
                                         </span>
                                         <strong>
                                          <span style="color: rgb(38, 123, 189)">&nbsp;
                                          </span>
                                          </strong>
                                          <strong>
                                            <span style="color: rgb(38, 123, 189)">؟</span>
                                            </strong>
                                            </p>
                                            <p style="text-align: right;">
                                            <strong>الخيار الأول:</strong> تفعيل جرس التنبيه والذي ينبهك كلما قاربت كمية المنتج على النفاد.</p>
                                            <p style="text-align: right;">
                                            <strong>الخيار الثاني:&nbsp;

                                            </strong>تعيين كمية المنتج إلى لا محدود.</p>
                                            <p style="text-align: right;">والخياران تجدهما يسار خانة كمية المنتج الموجود ضمن بطاقة المنتج، ويظهران بالشكل التالي:<br>
                                            <img src="" alt="photo"
                                             class="fr-fir fr-dib">
                                             </p>
                                             <p style="text-align: right;"><br></p><p style="text-align: right;"><strong><span style="color: rgb(38, 123, 189);">كيف يمكنني التحكم ببيانات المنتج؟</span>
                                             </strong>
                                             </p>
                                             <p style="text-align: right;">1. من صفحة المنتجات، اضغط على (بيانات المنتج).</p><p style="text-align: right;">
                                             <img src="" alt="photo" style="width: 350px;" class="fr-fic fr-dii">
                                             </p>
                                             <p style="text-align: right;">
                                             <br>
                                             </p>
                                             <p style="text-align: right;">2. من صفحة بيانات المنتج، أدخل البيانات المطلوبة لكلِّ منتجٍ، ثم اضغط على (حفظ)</p>
                                             <p style="text-align: right;">وتنقسم البيانات المطلوبة إلى:</p><p style="text-align: right;">
                                             <strong>1- تحديد خيار التوصيل.</strong></p><p style="direction: rtl; margin-right: 20px;"><img src="" alt="photo" width="409" height="99" class="fr-fic fr-dii"></p><ul style="margin-right: 20px; list-style-type: square;">
                                             <li style="direction: rtl; margin-right: 20px;">عندما يتطلب المنتج شحنًا: فهذا يعني أن العميل عندما يضيف المنتج إلى السلة، وينتقل إلى إتمام طلب الشراء فإنه سيتوقف حتماً عند خطوة اختيار شركة الشحن.</li>
                                             <li style="direction: rtl; margin-right: 20px;">أما عندما لا يتطلب المنتج شحنًا: فإن العميل سينتقل مباشرة إلى صفحة الدفع دون المرور بخطوة تحديد شركة الشحن.
                                            </li>
                                              </ul>
                                              <p style="direction: rtl; margin-right: 20px;">وبالتالي فإن تعيين هذا الخيار على قيمة (لا يتطلب شحن) لا يعني أنك لن تقوم بتوصيل المنتج إلى العميل، إنما تعني أنك لن تحتاج إلى شركة شحن لتوصيل المنتج إلى العميل.</p>
                                              <p style="text-align: right;">
                                                <br>
                                              </p>
                                              <p>
                                                <strong>2- تحديد وزن المنتج.</strong>
                                                </p>
                                                <p style="text-align: right; margin-right: 20px;">ويتم تقدير وزن المنتج بوحدة الكيلوجرام، وهو
                                                  <span >مطلوب لدى بعض شركات الشحن التي تعتمد على الوزن لحساب تكلفة الشحن؛ فلا بد من إدخاله بدقة.

                                                  </span>
                                                </p>
                                                <p style="text-align: right; margin-right: 20px;">

                                                  <img src="" style="width: 450px;" class="fr-fic fr-dii">
                                                </p>
                                                <p style="text-align: right; margin-right: 20px;">
                                                  <br>
                                                </p>
                                                <p>
                                                  <strong>3- إدخال رمز التخزين.</strong>
                                                </p><p style="margin-right: 20px;">
                                                  <span >رمز تخزين المنتج (SKU)، وهو رمز مهم للمتاجر التي تحتوي على مستودعات، وتحتاج إلى توصيف منتجاتها وترقيمها.</span></p><p style="margin-right: 20px;"><img src="" style="width: 450px;"
                                                     class="fr-fic fr-dii">
                                                    </p><p style="margin-right: 20px;">
                                                      <br>
                                                    </p>
                                                    <p>
                                                      <strong>4- تحديد سعر التكلفة.</strong>
                                                    </p>
                                                    <p style="margin-right: 20px;">سعر تكلفة المنتج، ومن المهم وضع سعر التكلفة؛ لأنه يساعدك في سياسة وضع التخفيضات على المنتجات في حال كنت تريد تجنب تجاوز التكلفة، ناهيك عن أهميته في
                                                      <span >معرفة أرباحك من خلال التقارير.</span>&nbsp;</p><p style="margin-right: 20px;">
                                                        <img src="" style="width: 450px;" class="fr-fic fr-dii">
                                                      </p><p style="margin-right: 20px;">
                                                        <br>
                                                      </p>
                                                      <p>
                                                        <strong>5- وضع السعر المخفض، وتحديد وقت لنهاية التخفيض.</strong>
                                                      </p>
                                                      <p style="direction: rtl; margin-right: 20px;">حدد سعر المنتج بعد التخفيض؛ في حال رغبت بعمل عروض وتخفيضات، مع إمكانية وضع تاريخٍ لانتهاء البيع بالسعر المخفض للمنتج.</p>
                                                      <p style="direction: rtl; margin-right: 20px;">
                                                        <img src=""  alt= "photo" style="width: 450px;" class="fr-fic fr-dii"></p><p style="direction: rtl; margin-right: 20px;">وسيظهر المنتج للعميل بالشكل التالي:</p><p style="direction: rtl; margin-right: 20px;"><img src="" alt= "photo"
                                                          style="width: 250px;" class="fr-fic fr-dii">
                                                        </p>
                                                        <p style="direction: rtl;">
                                                          <br>
                                                        </p>
                                                        <p>
                                                          <strong>6- تحديد أقصى كمية لكل عميل.</strong>
                                                        </p>
                                                        <p style="margin-right: 20px;"><span>تحديد أقصى كمية يمكن أن يطلبها العميل (اختياري).</span>
                                                        </p>
                                                        <p style="margin-right: 20px;"><img src="" slt="photo" style="width: 450px;" class="fr-fic fr-dii">

                                        </p>
                                        <p>
                                          <br>
                                        </p>
                                        <p>
                                          <strong>7- تحديد الماركة التجارية.</strong>
                                        </p><p style="margin-right: 20px;">
                                          <span >تحديد الماركة التي يندرج تحتها المنتج، وهذا خيار مفيد في عرض التقارير المتقدمة لمبيعات الماركات، وكذلك لتسهيل الوصول إلى منتجات ضمن ماركة معينة.</span>&nbsp;</p><p style="margin-right: 20px;">
                                            <img src="" alt="photo" style="width: 450px;" class="fr-fic fr-dii">
                                          </p>
                                          <p>
                                            <br>
                                          </p>
                                          <p>
                                            <strong>8- كتابة العنوان الفرعي</strong>
                                          </p><p style="margin-right: 20px;">العنوان الفرعي يظهر تحت اسم المنتج في المتجر.&nbsp;</p><p style="margin-right: 20px;">وسيظهر للعميل بالشكل التالي:</p><p style="margin-right: 20px;"><img src="" alt="photo " style="width: 250px;" class="fr-fic fr-dii">
                                          </p><p style="margin-right: 20px;">
                                            <br></p>
                                            <p style="margin-right: 20px;">ويمكنك كتابة 35 حرفًا وذلك الحد الأقصى.</p>
                                            <p style="margin-right: 20px;">
                                              <span>ولإضافة نسبة الخصم، أو مبلغ الخصم، أو اسم الماركة: اضغط على الزر الخاص بكل حقل من الحقول المطلوب إدراجها ضمن العنوان الفرعي.</span></p><p style="margin-right: 20px;">
                                                <img src="" alt="photo" style="width: 450px;" class="fr-fic fr-dii">
                                              </p>
                                              <p style="margin-right: 20px;">*هذه الميزة تضمن لك <span >تغير العنوان الفرعي</span> تلقائيًا في حال قمت بتغيير سعر المنتج أو اسم الماركة.
                                              </p>
                                              <p>
                                                <br>


                                              </p>
                                              <p>
                                                <strong>9- كتابة العنوان الترويجي</strong>
                                              </p>
                                              <p style="direction: rtl; margin-right: 20px;">هو عنوان يوضع على صورة المنتج، ويفيد في لفت انتباه العميل إلى ميزة، أو وصف، أو عرض على المنتج المعروض، كما في الشكل التالي:</p
                                                ><p style="direction: rtl; margin-right: 20px;"><img src="" alt="photo" style="width: 250px;" class="fr-fic fr-dii">
                                                </p><p style="direction: rtl; margin-right: 20px;"><br></p><p style="margin-right: 20px;">ويمكنك كتابة 35 حرفًا وذلك الحد الأقصى.</p><p style="margin-right: 20px;"><span>ولإضافة نسبة الخصم، أو مبلغ الخصم، أو اسم الماركة: اضغط على الزر الخاص بكل حقل من الحقول المطلوب إدراجها ضمن العنوان الفرعي.</span></p><p style="direction: rtl; margin-right: 20px;"><img src="" alt="photo" style="width: 450px;" class="fr-fic fr-dii">
                                                </p><p style="direction: rtl;">
                                                  <br>
                                                </p>
                                                <p>
                                                  <strong>10- تحديد كمية المنتج.</strong>
                                                </p>
                                                <p style="margin-right: 20px;">تعطيل أو تفعيل ظهور كمية المنتج للعميل أثناء تصفحه للمتجر (وهي ميزة مهمة للمتاجر التي تقدم خدمات لا تعتمد على تحديد الكمية المطلوبة من المنتج)</p>
                                                <p style="margin-right: 20px;"><img src="" style="width: 450px;"
                                                  class="fr-fic fr-dii">
                                                </p>
                                                <p>
                                                  <br>
                                                </p>
                                                <p>
                                                  <strong>11- إرفاق ملف عند الطلب.</strong>
                                                </p>
                                                <p style="margin-right: 20px;">السماح للعميل بإرفاق ملف عند طلب المنتج، وهي ضرورية للمتاجر التي تخدم الطلبات الخاصة، كالزخرفة والرسم وغيرها من أنواع الطلبات.</p>
                                                <p style="margin-right: 20px;"><img src="" alt="photo" style="width: 250px;" class="fr-fic fr-dii"></p><p style="margin-right: 20px;"><br></p><p style="margin-right: 20px;">وسيظهر للعميل بالشكل:</p>
                                                <p style="margin-right: 20px;"><img src="" style="width: 450px;" class="fr-fic fr-dii">
                                                </p>
                                                <p>
                                                  <br>
                                                </p>
                                                <p>
                                                  <strong>12- إمكانية كتابة ملاحظة.</strong>
                                                </p>
                                                <p>السماح للعميل بكتابة ملاحظاته عند طلب المنتج، بهدف تخصيص المنتج حسب توجيهات العميل.</p><p><img src=""
                                                  style="width: 250px;" class="fr-fic fr-dii">
                                                </p><p>
                                                  <br>
                                                </p>
                                                <p>وستظهر للعميل عندما يزور صفحة المنتج بالشكل التالي:</p>
                                                <p><img src="" alt="photo" style="width: 450px;" class="fr-fic fr-dii">
                                                </p>
                                                <p>
                                                  <br>
                                                </p>
                                                <p>
                                                  <strong>13- هل المنتج خاضع للضريبة؟</strong>
                                                </p><p style="margin-right: 20px;">
                                                  <span>تعيين ما إذا كان المنتج خاضعًا للضريبة أم لا.</span></p>
                                                  <p style="margin-right: 20px;"><img src="" style="width: 250px;" class="fr-fic fr-dii">
                                                  </p><p style="margin-right: 20px;"><br></p><p style="margin-right: 20px;">وستظهر ملاحظة للعميل تفيد خضوع المنتج للطريبة بجانب السعر:</p><p style="margin-right: 20px;"><img src="" alt="photo" style="width: 450px;" class="fr-fic fr-dii"></p><p><br></p><p><strong>14- كتابة وصف المنتج.</strong></p><p style="margin-right: 20px;">أدخل وصفًا تفصيليًا للمنتج. ويمكنك إضافة النصوص وتنسيقها، وكذلك إضافة والصور ومقاطع الفيديو والروابط وغير ذلك من الإمكانيات الرائعة للتنسيق.</p><p style="margin-right: 20px;"><img src="" alt="photo" style="width: 450px;" class="fr-fic fr-dii"></p><p style="margin-right: 20px;"><br></p><p style="margin-right: 20px;">سيظهر هذا الوصف للعميل عند فتح صفحة المنتج، بالشكل التالي:</p><p style="margin-right: 20px;"><img src=""  alt= "photo"style="width: 350px;" class="fr-fic fr-dii"></p><p><br></p><p><strong>15. إدخال وسوم المنتج.</strong></p><p style="margin-right: 20px;">ويمكن إدخال عدة وسوم للمنتج بكتابة الوسم المطلوب ثم الضغط على (إضافة)</p><p style="margin-right: 20px;"><img src=""alt="photo" style="width: 450px;" class="fr-fic fr-dii"></p><p style="margin-right: 20px;"><br></p><p style="margin-right: 20px;">للمزيد حول وسوم المنتجات: اضغط هنا.</p><p><br></p><p><strong>16- الاستفادة من تحسينات SEO.</strong></p><p style="margin-right: 20px;">تحسين صفحات المنتجات في محركات البحث.</p><p style="text-align: right; margin-right: 20px;"><img src="" alt="photo" style="width: 450px;" class="fr-fic fr-dii"></p><p style="text-align: right; margin-right: 20px;"><br></p><p style="direction: rtl; margin-right: 20px;">للمزيد حول <a href=""><strong>أساسيات تحسين الظهور على محركات البحث SEO</strong></a></p><p style="text-align: right;"><br></p><p><strong>
                                                    <span style="color: rgb(38, 123, 189);">كيف يمكنني تثبيت منتج في الصفحة الرئيسية؟</span></strong></p><p>من صفحة المنتج، اضغط زر تثبيت المنتج، كما في الشكل التالي:</p><p><img src="" alt="photo" style="width: 300px;" class="fr-fic fr-dii"></p><p>ويمكنك الضغط على زر تثبيت المنتج مرة أخرى لإلغاء تثبيت المنتج من الصفحة الرئيسية.</p>
                                                    <p><br></p><p style="text-align: right;">
                                                    <span style="color: rgb(38, 123, 189);"><strong>ما هي الخيارات التي يتيحها لي زر المزيد؟</strong>
                                                    </span></p><p style="text-align: right;">يتيح لك زر المزيد العديد من الخيارات التي تحتاجها، وهي:</p><p style="text-align: right;">
                                                    <img src="" style="width: 250px;" class="fr-fic fr-dii"></p><p style="text-align: right;"><br></p><p style="text-align: right;">لمزيد من التفاصيل حول <a href="">
                                                    <strong>حول زر (المزيد) من صفحة المنتج</strong></a>.
                                                  </p><p><br></p></div>


</body>

</html>
