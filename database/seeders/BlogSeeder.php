<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Category::insert([
            ['name' => 'اعداد الفاتورة لاول مره'],
            ['name' => 'المنتجات'],
            ['name' => 'الطلبات'],
            ['name' => 'طرق الدفع'],
            ['name' => 'المحفظة'],
            ['name' => 'خيارات الشحن والتوصيل'],
            ['name' => 'الربط مع شركات الشحن'],
            ['name' => 'العملاء'],
            ['name' => 'الاسلئة والتقييمات'],
            ['name' => 'التقارير'],
            ['name' => 'الصفحات التعلايفية'],
            ['name' => 'مدونة المتجر'],
            ['name' => 'كوبونات التخفيض'],
            ['name' => 'العروض الخاصه'],
            ['name' => 'التسويق والحملات التسويقية'],
            ['name' => 'الفواتير المتروكة'],
            ['name' => 'ميزات باقه سبشل'],
            ['name' => 'موظفو المتجر'],
            ['name' => 'دومين الفاتورة'],
            ['name' => 'التحليل والاحصاء'],
            ['name' => 'نظام الولاء'],
            ['name' => 'إعدادات المتجر'],
        ]);

        Blog::insert([
            ['title' => 'تسجيل حساب جديد', 'cat_id'=> 1],
            ['title' => 'انشاء متجر جديد على فاتورة', 'cat_id'=> 1],
            ['title' => 'اضافة اول منتج', 'cat_id'=> 1],
        ]);
    }
}
