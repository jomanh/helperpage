<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function storeMedia(Request $request , $table)
    {

        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();

        $name =  "";
        $path = "";
        switch ($table){



            case "blogs":
                $name = "blogs". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
                $path = storage_path('app/public/backend/blogs/uploads');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $name = "blogs/uploads/".$name;
                break;
            case "categories":
                $name = "categories". uniqid() . '_' .rand(1,1000000). ".". strtolower($ext);
                $path = storage_path('app/public/backend/category/uploads');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $name = "category/uploads/".$name;
                break;



}

$file->move($path, $name);

return response()->json([
    'name'          => $name,
    'original_name' => $file->getClientOriginalName(),
    "img2"=>str_replace("/","_",$name)
]);
}

public function destroyMedia($table, Request $request){
    switch ($table){
        // case "users":
        //     $item = User::find($request->user_id);
        //     $images = explode(",",$item->images);
        //     $key = array_search($request->image, $images);
        //     if($key !== false){

        //         unset($images[$key]);
        //     }
        //     $images =implode(",",$images);

        //     User::where('id','=',$request->user_id)->update(['images'=>$images]);
        //     return response()->json(["status"=>200,"images" =>$images, "img"=>$request->image , "img2"=>str_replace("/","_",$images)]);
        //     break;

    }

}

}
