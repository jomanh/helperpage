<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\allTrait;
use Illuminate\Support\Facades\Validator;
use \Yajra\Datatables\Datatables;
use App\Models\Blog;
use App\Models\Category;


class BlogController extends Controller
{
    use allTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        if ($request->ajax()) {

            $data = Blog::with('cat')->get();

            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('action', function ($row) {

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit"> <i class="fa fa-edit"></i> </a>';


                    $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-danger btn-sm delete"> <i class="ti-trash"></i> </a>';

                    return $btn;
                })->addColumn('image', function ($row) {

                    return "<img src='" . asset('storage/backend/' . $row->image) . "' width='50' height='50'>";
                })

                ->rawColumns(['action', 'image'])

                ->make(true);
        }

        $categoriesNames = Category::get();
        return view("backend.blogs", compact('categoriesNames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateErrors = Validator::make(
            $request->all(),
            [
                'title' => 'required|string|min:3',
            ]
        );
        if ($validateErrors->fails()) {
            return response()->json(['status' => 201, 'message' => $validateErrors->errors()->first()]);
        }
        $data = [
            'cat_id' => $request->cat_id,
            'title' => $request->title,
            'description' => $request->description,
        ];
        if (!empty($request->image)) {
            $data['image'] = $request->image;
        }
        $id =  Blog::updateOrCreate(['id' => $request->_id], $data)->id;
        return response()->json(['status' => 200, 'message' => ' تم حفظ البيانات  بنجاح .', "data" => null]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        return  $this->editController($id, Blog::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
            'Catagory_id' => $request->type_id,
            'title' => $request->title,
            'description' => $request->description,
        ];
        if (!empty($request->image)) {
            $data['image'] = $request->image;
        }
        Blog::updateOrCreate(['id' => $request->_id], $data);
        return response()->json(['status' => 200, 'message' => ' saved data successful    .']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->destroyController($id, Blog::class);
    }
}
