<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory, SoftDeletes;

    protected $table="blogs";
    protected $fillable = ["cat_id","title","description","image" ];

    public function cat(): BelongsTo
    {
        return $this->belongsTo('App\Models\Category','cat_id');
    }

}
