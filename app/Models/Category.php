<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $table="categories";
    protected $fillable = ["name","image" ];

    public function blogs(): HasMany
    {
        return $this->hasMany(Blog::class,'cat_id');
    }
}
